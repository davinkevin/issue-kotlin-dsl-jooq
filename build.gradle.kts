import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.rohanprabhu.gradle.plugins.kdjooq.*

plugins {
	id("org.springframework.boot") version "2.3.1.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	id("com.rohanprabhu.kotlin-dsl-jooq") version "0.4.5"
	kotlin("jvm") version "1.3.72"
	kotlin("plugin.spring") version "1.3.72"
}

group = "com.gitlab.davinkevin.issue"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-jooq")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	implementation("org.postgresql:postgresql:42.2.12")
	jooqGeneratorRuntime("org.postgresql:postgresql:42.2.12")

	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

jooqGenerator {
	jooqVersion = "3.13.2"

	configuration("primary", project.sourceSets.getByName("main")) {

		configuration = jooqCodegenConfiguration {
			jdbc {
				url = "jdbc:postgresql://postgres:5432/podcast-server"
				username = "podcast-server-user"
				password = "test"
				driver = "org.postgresql.Driver"
			}

			generator {

				database {
					name = "org.jooq.meta.postgres.PostgresDatabase"
					inputSchema = "public"
				}

				target {
					packageName = "com.github.davinkevin.podcastserver.database"
					directory = "${project.buildDir}/generated/jooq/primary"
				}
			}
		}
	}
}
