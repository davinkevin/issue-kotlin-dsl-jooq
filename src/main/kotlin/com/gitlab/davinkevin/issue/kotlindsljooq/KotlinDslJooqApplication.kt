package com.gitlab.davinkevin.issue.kotlindsljooq

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinDslJooqApplication

fun main(args: Array<String>) {
	runApplication<KotlinDslJooqApplication>(*args)
}
